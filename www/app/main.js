
(function () {
    'use strict';
    
    angular
            .module('entapp', [
                'ui.router',
                'ngCordova',
                'ngDialog',
                'entapp.common',
                'entapp.home'
            ]);

})();