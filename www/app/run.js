(function () {

    angular
            .module('entapp')
            .run(run);
    run.$inject = ['$rootScope'];

    function run($rootScope) {
        $rootScope.varTest = 'Hello, this is a test variable.'
    }

})();